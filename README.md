# geomsg
Plataforma de compartilhamento de mensagens geolocalizadas.
# server
.net web api, sql server, entity framework, custom token authentication

# client
vue.js, vuetify.js, axios, leaflet.js
